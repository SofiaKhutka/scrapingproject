/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import News from './components/News'

class App extends Component {
  state = {
    news: [],
    page:1
  }
  fetchData=(pageNum)=>{
    const requestBody = {
      query: `
      query{
        news(page: ${pageNum}){
          _id
          title
          description
          date
          image
        }
      }
      `
    };
    return fetch('http://localhost:3000/graphql', {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res =>{
      if(res.status!==200 &&res.status!==201) {
        throw new Error('Failed');
      }
      return res.json();
    })
    .then(resData => {
      console.log('RESULT!', resData.data)
      return resData.data
    })
    .catch(err =>{
      console.log(err);
    })
  }
  componentDidMount = ()=>{
    return this.fetchData(1)
    .then(data => {
      return this.setState({
        news: data.news
      })
    })
  }

  renderNews = (item) => {
    return (
      <News
        title={item.title}
        description={item.description}
        date={item.date}
        image={item.image}
        id={item._id}
        key={item._id.toString()}
      />
    )
  }
  nextPage=()=>{
    return this.fetchData(this.state.page)
    .then(data => {
      return this.setState(prevState=>{return {
        news: data.news,
        page: prevState.page+1
      }})
    })
  }
  componentDidUpdate=()=>{
    this.ListView_Ref.scrollToOffset({ offset: 0,  animated: true });
  }
  render(){
    
  return (
    <View style={{flex:1}}>
    <FlatList
    ref={(ref) => {
      this.ListView_Ref = ref;
    }}
      style={styles.listItems}
      data={this.state.news}
      keyExtractor={item=>item._id}
      renderItem ={({item})=>this.renderNews(item)}
      //onEndReached={()=>this.nextPage()}
    />
    <TouchableOpacity style={styles.next}onPress={()=>this.nextPage()}>
      <Text>
      NEXT
      </Text>
    </TouchableOpacity>
    </View>
  );}
};

const styles = StyleSheet.create({
  next:{
    height: '3%',
    alignItems:"center",
    justifyContent:"center",
    marginBottom: '3%'
  },
  container: {
    marginTop: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  listItems: {
    flex:1,
    marginTop: 60,

    //alignItems: 'center',
  
  }
});

export default App;
