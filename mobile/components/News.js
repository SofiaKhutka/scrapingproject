import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  Button
} from 'react-native';
//import Icon from "react-native-vector-icons/Entypo";


class News extends Component{
    render(){
        return(
            <View>
            <View style={styles.listItemBox}>
                <View style={styles.imageContainer}>
                <Image 
                style={{
                    width: 170,
                    height: 180,
                    borderTopLeftRadius: 15,

                  }}
                source={{uri: this.props.image}}/></View>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>
                        {this.props.title.toUpperCase()}
                    </Text>
                    <Text
                        numberOfLines={4}
                    >
                        {this.props.description}
                    </Text>
                    
                    </View>
            </View>
            <View style={styles.buttons}>
                  <Button
                    title={'like'.toUpperCase()}
                  />
                  <Button
                    title={'dislike'.toUpperCase()}
                  />
            </View>
            </View>
        )
    }
}
const styles = new StyleSheet.create({
    listItemBox: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: '97%',
        flex:1,
        marginHorizontal: '2%',
        height: 180,
        backgroundColor: '#d4caff',
        marginTop: '2%',
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        //borderBottomColor: "#5b45b9",
        // borderBottomWidth: 3,
        justifyContent: "space-between",
        display: 'flex',
        
    },
    textContainer: {
        marginRight: '3%',
        marginLeft: '1%',
        width: '56%'
    },
    text: {
        marginVertical:"4%",
        fontWeight: '700',
        fontSize: 15
    },
    buttons:{
        backgroundColor: '#d4caff',

        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '97%',
        flex:1,
        marginHorizontal: '2%',
        borderBottomColor: "#5b45b9",
        borderTopColor: "white",
        //marginTop: 3,
        borderBottomWidth: 3,
        borderTopWidth: 3,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20



    }
    //imageContainer: {flex:1}
})

export default News;