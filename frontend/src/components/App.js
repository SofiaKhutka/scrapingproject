import React, {Component} from 'react';
import News from './News'

class App extends Component {
  state = {
    page: 1,
    news: [
      // {
      //   title: 'hello',
      //   description: "sofia",
      //   _id: 1,
      //   date: "April 17, 3:41 pm",
      //   image: "https://www.kyivpost.com/wp-content/uploads/2020/04/OLE_9898-300x200.jpg"
      // },
      // {
      //   title: 'hello',
      //   description: "sofia",
      //   _id: 2,
      //   date: "April 17, 3:41 pm",
      //   image: "https://www.kyivpost.com/wp-content/uploads/2020/04/OLE_9898-300x200.jpg"
      // },
      // {
      //   title: 'hello',
      //   description: "sofia",
      //   _id: 1,
      //   date: "April 17, 3:41 pm",
      //   image: "https://www.kyivpost.com/wp-content/uploads/2020/04/OLE_9898-300x200.jpg"
      // },
      // {
      //   title: 'hello',
      //   description: "sofia",
      //   _id: 2,
      //   date: "April 17, 3:41 pm",
      //   image: "https://www.kyivpost.com/wp-content/uploads/2020/04/OLE_9898-300x200.jpg"
      // },
      
    ]
  }

  fetchData=(pageNum)=>{
    const requestBody = {
      query: `
      query{
        news(page: ${pageNum}){
          _id
          title
          description
          date
          image
        }
      }
      `
    };
    return fetch('http://localhost:3000/graphql', {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res =>{
      if(res.status!==200 &&res.status!==201) {
        throw new Error('Failed');
      }
      return res.json();
    })
    .then(resData => {
      console.log('RESULT!', resData.data)
      return resData.data
    })
    .catch(err =>{
      console.log(err);
    })
  }
  componentWillMount = ()=>{
    return this.fetchData(1)
    .then(data => {
      return this.setState({
        news: data.news
      })
    })
  }
  nextPage = ()=> {
    return this.fetchData(this.state.page)
    .then(data => {
      return this.setState(prevState=>{ 
        return{
          news: data.news,
          page: prevState.page+1
        }})
    })
  }
  render() {
    window.scrollTo(0, 0)
    console.log(this.state);
    return (
      <div>
      <div className="newspaper">
        {this.state.news.map((news, index) => (
          <News
            title={news.title}
            description={news.description}
            date={news.date}
            image={news.image}
            id={news._id}
            key={news._id.toString()}
            index={index}
          />
        ))}
        <button className="next" onClick={()=>this.nextPage()}>
        NEXT
        </button>
      </div>
      </div>
    );
  }
}

export default App;
