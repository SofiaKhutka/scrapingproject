import React, { PureComponent } from "react";
import {FaThumbsUp, FaThumbsDown} from 'react-icons/fa'
//import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'


class News extends PureComponent {
  buttonclicked = ()=>{
    const requestBody = {
      query: `
      query{
        news{
          description
        }
      }
      `
    };
    fetch('http://localhost:3000/graphql', {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res =>{
      if(res.status!==200 &&res.status!==201) {
        throw new Error('Failed');
      }
      return res.json();
    })
    .then(resData => console.log('RESULT!', resData))
    .catch(err =>{
      console.log(err);
    })
  }
  render() {
    return (
      <div className="news">
        <img src={this.props.image}/>
        <div className="news-container">
          {/* <button
            className="remove-player"
            onClick={() => {
              //this.props.removePlayer(this.props.id);
              console.log('cross');
              this.buttonclicked();
            }}
          >
            ✖
          </button> */}
          
          <h4>
          {this.props.title}
          </h4>
          <p>
          {this.props.description}
          </p>
        </div>
        <div className="evaluate">
          <FaThumbsUp className="vote" size={70}/>
          <FaThumbsDown className="vote"/>
        </div>
      </div>
    );
  }
}
export default News;
