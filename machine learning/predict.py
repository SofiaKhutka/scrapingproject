#!/usr/bin/env python
# coding: utf-8

# In[134]:


import pandas as pd
from nltk.tokenize import word_tokenize

columns = ['text', 'like']
rows = []

from pymongo import MongoClient
client = MongoClient("localhost", 27017)
db = client["scrap"]
data = db['news'].find({"like": {'$nin': ['null', None]}}, {"title": 1, "description": 1, "like": 1})
rows=[]
for post in data:
    title = post.get('title', ' ').lower()
    like = 'true' if post.get('like') else 'false'
    row=[title,like]
    rows.append(row)

training_data = pd.DataFrame(rows, columns=columns)
training_data


# In[135]:


from sklearn.feature_extraction.text import CountVectorizer

stmt_docs = [row['text'] for index,row in training_data.iterrows() if row['like'] == 'true']

vec_s = CountVectorizer()
X_s = vec_s.fit_transform(stmt_docs)
tdm_s = pd.DataFrame(X_s.toarray(), columns=vec_s.get_feature_names())

tdm_s


# In[ ]:





# In[136]:


q_docs = [row['text'] for index,row in training_data.iterrows() if row['like'] == 'false']

vec_q = CountVectorizer()
X_q = vec_q.fit_transform(q_docs)
tdm_q = pd.DataFrame(X_q.toarray(), columns=vec_q.get_feature_names())

tdm_q


# In[137]:


word_list_s = vec_s.get_feature_names();    
count_list_s = X_s.toarray().sum(axis=0) 
freq_s = dict(zip(word_list_s,count_list_s))
freq_s


# In[138]:


word_list_q = vec_q.get_feature_names();    
count_list_q = X_q.toarray().sum(axis=0) 
freq_q = dict(zip(word_list_q,count_list_q))
freq_q


# In[139]:


prob_s = []
for word,count in zip(word_list_s,count_list_s):
    prob_s.append(count/len(word_list_s))
dict(zip(word_list_s,prob_s))


# In[140]:


prob_q = []
for count in count_list_q:
    prob_q.append(count/len(word_list_q))
dict(zip(word_list_q,prob_q))


# In[141]:


from sklearn.feature_extraction.text import CountVectorizer

docs = [row['text'] for index,row in training_data.iterrows()]

vec = CountVectorizer()
X = vec.fit_transform(docs)

total_features = len(vec.get_feature_names())
total_features


# In[142]:


total_cnts_features_s = count_list_s.sum(axis=0)
total_cnts_features_q = count_list_q.sum(axis=0)


# In[143]:


def predictText(text):
    new_sentence=text
    new_word_list = word_tokenize(new_sentence)
    prob_s_with_ls = []
    for word in new_word_list:
        if word in freq_s.keys():
            count = freq_s[word]
        else:
            count = 0
        prob_s_with_ls.append((count + 1)/(total_cnts_features_s + total_features))
    p_words_1=dict(zip(new_word_list,prob_s_with_ls))

    prob_q_with_ls = []
    for word in new_word_list:
        if word in freq_q.keys():
            count = freq_q[word]
        else:
            count = 0
        prob_q_with_ls.append((count + 1)/(total_cnts_features_q + total_features))
    p_words_2=dict(zip(new_word_list,prob_q_with_ls))

    probability_result_1=0.5
    for word in new_word_list:
        pi1=p_words_1[word]
        probability_result_1=probability_result_1*pi1

    probability_result_2=0.5
    for word in new_word_list:
        pi2=p_words_2[word]
        probability_result_2=probability_result_2*pi2

    result='liked' if probability_result_2<probability_result_1 else 'disliked'
    print(text)
    print(result)
    return result


# In[ ]:





# In[ ]:





# In[ ]:





# In[146]:


def saveToDb(ids,predictions):
    for number, id in enumerate(ids):
        if predictions[number] == "liked":
            db['news'].find_one_and_update({"_id": id}, {'$set': {"willLike": True}})
        else:
            db['news'].find_one_and_update({"_id": id}, {'$set': {"willLike": False}})
    print('saved')


# In[147]:


data = db['news'].find({})
ids=[]
predictions=[]
for post in data:
    title = post.get('title', ' ').lower()
    id= post.get('_id', ' ')
    ids.append(id)
    likePredict=predictText(title)
    predictions.append(likePredict)
saveToDb(ids,predictions)


# In[ ]:





# In[ ]:




