const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const newsSchema = new Schema({
    title: String,
    description: String,
    date: String,
    image: String,
    vote: String
})
module.exports = mongoose.model("News", newsSchema);
