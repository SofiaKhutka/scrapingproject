var amqp = require('amqplib/callback_api');
const mongoose = require("mongoose");
const writeInfo = require('./writeToDb')

mongoose.connect("mongodb://localhost:27017/scrap", {
  useNewUrlParser: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function() {
  console.log(" We're connected!");
});


amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'hello';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function(msg) {
            //console.log(" [x] Received %s", msg.content);
            //console.log('nanana', typeof msg.content);
            console.log(JSON.parse(msg.content).i)
            writeInfo(JSON.parse(msg.content).i);
        }, {
            noAck: true
        });
    });
});