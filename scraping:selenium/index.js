var webdriver = require ('selenium-webdriver'),
  By = webdriver.By;
var amqp = require('amqplib/callback_api');
require('chromedriver');


(async function main(){
  let information = await scrapInformation();
  //console.log('\n\nINFO \n', information);
  information = onlyNew(information)
  console.log('new count', information.length)
  console.log('only new', information);

  information = {i: information}
  information = JSON.stringify( information)


  amqp.connect('amqp://localhost',  function(error0, connection) {
    if (error0) {
        throw error0;
    }
    console.log('loading...');
    connection.createChannel(  function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var queue = 'hello';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(information));

        console.log(" [x] Sent %s", information);
    });
    setTimeout(function() {
        connection.close();
        process.exit(0);
    }, 500);
  });

})();

function onlyNew(arr){
  let latestPost = Date.parse('April 17, 3:27 pm')
  return arr.filter(newsPost => {
    let date = Date.parse(newsPost.date)
    return date>latestPost;
  });
}


async function scrapInformation(){
  var driver =  new webdriver.Builder()
    .forBrowser('chrome')
    .build();
  console.log('executing...');

  let news=[]
  await driver.get("https://www.kyivpost.com/all-news/");

  ///html/body/div[2]/main/div/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]

  await driver.findElements(By.className('post-excerpt post-excerpt-simple post-excerpt-list-style'))
    .then(async (elements) => {
        for (let i = 0; i < elements.length; i++){
          let newsPost={};
          await elements[i].findElement(By.className('title')).getText().then((text)=> {newsPost.title=text})
          await elements[i].findElement(By.className('short-description')).getText().then((text)=> {newsPost.description=text})
          await elements[i].findElement(By.className('post-date')).getText().then((text)=> {newsPost.date=text})
          await elements[i].findElement(By.className('article-img load_img complete_img show')).getAttribute("data-img-url").then((text)=> {newsPost.image=text})
          news.push(newsPost)
        };
    })
  // let newsPost ={}
  // await driver.findElement(By.className('post-excerpt post-excerpt-simple post-excerpt-list-style'))
  //   .then(async(elem)=> {
  //     await elem.findElement(By.className('title')).getText().then((text)=> {newsPost.title=text})
  //     await elem.findElement(By.className('short-description')).getText().then((text)=> {newsPost.description=text})
  //     await elem.findElement(By.className('post-date')).getText().then((text)=> {newsPost.date=text})
  //     await elem.findElement(By.className('article-img load_img complete_img show')).getAttribute("data-img-url").then((text)=> {newsPost.image=text})


//    })
    .catch(error => console.log(error));
    //console.log('!!', news);

  return news;
};

