const News = require("./models/News");
const mongoose = require("mongoose");

module.exports = function(arrayOfNews){
    // const news = new News({
    //     id: new mongoose.Types.ObjectId(),
    //     title: "Ominous silence",
    //     description:
    //  'Almost three weeks have passed since video footage implicating the brother of President Volodymyr Zelensky’s chief of staff in corruption was …',
    // date: 'April 17, 1:03 pm',
    // image:
    //  'https://www.kyivpost.com/wp-content/uploads/2019/04/Kyiv-Post-Editorial-1-300x200.png' 
    // })
    // news.save(function(err, result) {
    //     if (err) console.log(err);
    //     else console.log("Data inserted!", result);
    //   });
    News.insertMany( arrayOfNews );

}