const express = require('express');
const bodyParser = require('body-parser');
const graphqlHttp = require('express-graphql');
const { buildSchema }= require('graphql');
const mongoose = require("mongoose");


const News = require('../scraping:selenium/models/News')

const app = express();

app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/scrap", {
  useNewUrlParser: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function() {
  console.log(" We're connected!");
});

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if(req.method === 'OPTIONS'){
        return res.sendStatus(200);
    }
    next();

})

app.use('/graphql', graphqlHttp({
    schema: buildSchema(`
        type News{
            _id: ID!
            title: String!
            description: String!
            date: String!
            image: String!
        }
        
        type RootQuery {
            news(page: Int!): [News!]!
        }
        type RootMutation {
            setLike(vote: String, id: String): String
        }
        schema{
            query: RootQuery
            mutation: RootMutation
        }
    `),
    rootValue: {
        news: (args)=>{
            let skipnum = (args.page-1)*10
            return News.find().limit(10).skip(skipnum)
            .then(news => {
                console.log('FOUND', news);
                return news.map(info =>{
                    return {...info._doc, _id: info._doc._id.toString()}
                });
            })
            .catch(err=>{
                console.log('ERROR');
                throw err;
            })
        },
        setLike: async (args)=>{
            console.log('muatted');
            const vote = args.vote;
            const id = args.id
            //return eventName;
            const filter = { id: id };
            const update = { vote: vote };

            let doc = await News.findOneAndUpdate(filter, update, {new: true} );
            //await doc.save();
            return vote;
        }
    },
    graphiql: true
}));

app.listen(3000)